/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuradesdecontrol;

import javax.swing.JOptionPane;

/**
 *
 * @author dell
 */
public class Main {

    public static void main(String[] args) {
        Calculadora cal = new Calculadora();
        String menu = "Menu Principal - UTN v0.1\n"
                + "1. Sumar\n"
                + "2. Restar\n"
                + "3. Multiplicar\n"
                + "4. Dividir\n"
                + "5. Salir\n";

        int op = Integer.parseInt(JOptionPane.showInputDialog(menu));
       
        switch (op) {
            case 1:
                int suma1 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un numero para Suma: "));
                int suma2 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un numero para Suma: "));
                int totalSuma = cal.suma(suma1, suma2);
                JOptionPane.showMessageDialog(null, "La Suma de: " + suma1+" + "+suma2+" = "+totalSuma);
                break;
            case 2:
                int resta1 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un numero para Resta: "));
                int resta2 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un numero para Resta: "));
                int totalResta = cal.resta(resta1, resta2);
                JOptionPane.showMessageDialog(null, "La Resta de:  " + resta1+" - "+resta2+" = "+totalResta);
                break;
            case 3:
                int multi1 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un numero para Multiplicación: "));
                int multi2 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un numero para Multiplicación: "));
                int totalMulti = cal.multiplicar( multi1,multi2);
                JOptionPane.showMessageDialog(null, "La Multiplicación de:  " + multi1+" * "+multi2+" = "+totalMulti);
                break;
            case 4:
                int dividir1 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un numero para Dividir: "));
                int dividir2 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un numero para Dividir: "));
                int totalDividir = cal.dividir(dividir1, dividir2);
                JOptionPane.showMessageDialog(null, "La Divición de:  " + dividir1+" / "+dividir2+" = "+totalDividir);
                break;
            case 5:
                JOptionPane.showMessageDialog(null, "Muchas Gracias por utilizar nuestra app");
                break;

      
            default:
                JOptionPane.showMessageDialog(null, "Opción Invalida");
        }

        
        }
}
