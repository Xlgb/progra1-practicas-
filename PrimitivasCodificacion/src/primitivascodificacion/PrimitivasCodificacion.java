/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package primitivascodificacion;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author dell
 */
public class PrimitivasCodificacion {

    public static void main(String[] args) {
        //Tipo de datos !!!
        System.out.println("A\tB\tC\n1\t2\t3\n");
        System.out.println("\"HOLA\"");
        System.out.println("El caracter de escape \\n cambia de linea");
        System.out.println("EL UNICODE \\u0040 vale \u0040");
        System.out.println("EL UNICODE \\u00D1 vale \u00D1");
        
        
        //Primitivas de codificacion!!
        
        //Variables 
        
        int notaFinal = 98;//Declararla = tipo + nombre y inicializarla = ponerle un valor.
        // variable constante 
        final double VALOR_PI = 3.141592;
        
        // Ejercicios
        boolean isReal = true;
        byte b = 122;
        short s = -29000;
        int i = 100000;
        long l = 999999999999L;
        float f1 = 234.99F;
        double d;
        char cvalue = 's';
        final double PI = 3.141592;
        //Casting
        int cast = (int)22.32;
        double d1 = 12; // En un double se puede poner un entero !!
        double c = (double)5/2;
        
        // Operadores
        System.out.println("\nOperadores");
        System.out.println("12+12=" + (12+12));
        System.out.println("12-2=" + (12-2));
        System.out.println("12*2="+(12*2));
        System.out.println("12/2="+(12/2));
        System.out.println("5/2=" + (5/2));// Si los dos operadores son enteros el resultado es entero.
        
        int cont = 1;
        int d3 = cont++;
        System.out.println("Cont:" + cont);
        System.out.println("d3: " + d3);
        // o se pued usar +=
        int cont1 = 1;
        int d4 = --cont;
        System.out.println("Cont:" + cont1);
        System.out.println("d4: " + d4);
        
        // Operadores de asignacion
        cont = 10;
        cont /=5;
        System.out.println("cont" + cont);
        
        // cont = cont + 5; //cont += 5;
        
        
        //Scanner sc = new Scanner(System.in);
        //System.out.print("Digite su nombre :");
        //String nom = sc.nextLine();
        //System.out.println(nom);
        
        //System.out.println("Digite su edad"); 
        //int a = Integer.parseInt(sc.nextLine());
        //System.out.println("Edad: " + a);
        
        //Mostrar un mensaje
        //String nombre = JOptionPane.showInputDialog("Digite su nombre :");
        //int edad = Integer.parseInt(JOptionPane.showInputDialog("Digite su Edad"));
        //JOptionPane.showMessageDialog(null, "Nombre : " + nombre + "\nedad : " + edad, "Ejemplo", JOptionPane.PLAIN_MESSAGE);
        
        
        //Ejemplos de como usar JOptionPane
        //#1
        //JOptionPane.showMessageDialog(null,"Hola Mundo" , "Casa", JOptionPane.INFORMATION_MESSAGE);
        //#2
        //JOptionPane.showMessageDialog(null,"Error!" , "ERROR", JOptionPane.ERROR_MESSAGE);
        //#3
        //JOptionPane.showMessageDialog(null,"Cuidado" , "Advertencia", JOptionPane.WARNING_MESSAGE);
        //#4
        //JOptionPane.showMessageDialog(null,"Sin ICONO" , "Vuelve a intentar", JOptionPane.PLAIN_MESSAGE);
        
        Prueba p = new Prueba();
        int x = p.suma(2, 2);
        System.out.println(x);
        System.out.println(p.esPar(12));
        
        
      
         
    }
    
    }
