/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expresiones;

import javax.swing.JOptionPane;

/**
 *
 * @author allanmual
 */
public class EjemploSuma {

    public static int suma(int n1, int n2) {
        int total = n1 + n2;
        return total;
    }

    public static String mayor(int n1, int n2) {
        if (n1 > n2) {
            //Bloque de instrucciones cuando es verdadero
            return "N1 si es mayor que N2";
        } else {
            //Bloque de instrucciones cuando es falso
            return "N1 no es mayor que N2";
        }
    public static String positivo(int n3) {
        if (n3 < 0) {
            //Bloque de instrucciones cuando es verdadero
            return " es negativo\n";
        } else {
            //Bloque de instrucciones cuando es falso
            return "N1 no es mayor que N2";
        }
    }

    public static void main(String[] args) {
        //Pide dos números
//        Scanner sc = new Scanner(System.in);
//        System.out.print("#1: ");
//        int n1 = Integer.parseInt(sc.nextLine());
//        System.out.print("#2: ");
//        int n2 = Integer.parseInt(sc.nextLine());
//        //Suma los dos números
//        int total = n1 + n2;
//        //Muestra el resultado
//        System.out.println(n1 + "+" + n2 + "=" + total);

        int a = Integer.parseInt(JOptionPane.showInputDialog("#1"));
        int b = Integer.parseInt(JOptionPane.showInputDialog("#2"));
//        //Suma los dos números
        int t = suma(a, b);
//        //Muestra el resultado
        JOptionPane.showMessageDialog(null, a + "+" + b + "=" + t);

        
        
        String msj = mayor(a, b);
        System.out.println(msj);
        
        System.out.println(mayor(a, b));
        
        

//Pedir un número(puede tener decimales)
//        double n3 = Double.parseDouble(JOptionPane.showInputDialog("Digite un número"));
//        String res = "";
//        //Determinar si un número es negativo o positivo
//
//        if (n3 < 0) {
//            res = n3 + " es negativo\n";
//        } else {
//            res = n3 + " es positivo\n";
//        }
//
//        if (n3 % 5 == 0) {
//            res += n3 + " es divisible entre 5";
//        }
//        JOptionPane.showMessageDialog(null, res);
        //Pedir un número entero y determinar si es par o no.
//        String txt = JOptionPane.showInputDialog("Digite un número");
//        int n4 = Integer.parseInt(txt);
//        
//        if (n4 % 2 == 0) {
//            JOptionPane.showMessageDialog(null, n4 + " es par");
//        } else {
//            JOptionPane.showMessageDialog(null, n4 + " es impar");
//        }
//        int x = Integer.parseInt(JOptionPane.showInputDialog("Digite el valor para x"));
//        int y = Integer.parseInt(JOptionPane.showInputDialog("Digite el valor para y"));
//        if (x > y) {
//            System.out.println("x > y " + "(x: " + x + ", y: " + y + ")");
//        } else if (x < y) {
//            System.out.printf("x < y (x: %d, y: %d)\n", x, y);
//        } else {
//            System.out.printf("x = y (x: %d, y: %d)\n", x, y);
//        }
//        int ale = (int)(Math.random()*6)+1;
//        System.out.println(ale);
//        int costo = Integer.parseInt(JOptionPane.showInputDialog("Costo: "));
//        int ejes = Integer.parseInt(JOptionPane.showInputDialog("Ejes: "));
//        int pasa = Integer.parseInt(JOptionPane.showInputDialog("Pasajeros: "));
//
//        double impuesto = costo * 0.01; //100
//        double aumEjes = 0; // 5
//        double aumPasa = 0; // 1
//
//        if (pasa < 20) {
//            aumPasa = impuesto * 0.01;
//        } else if (pasa >= 20 && pasa <= 60) {
//            aumPasa = impuesto * 0.05;
//
//        } else {
//            aumPasa = impuesto * 0.08;
//        }
//
//        if (ejes == 2) {
//            aumEjes = impuesto * 0.05;
//        } else if (ejes == 3) {
//            aumEjes = impuesto * 0.10;
//        } else if (ejes > 3) {
//            aumEjes = impuesto * 0.15;
//        }
//
//        double cf = costo + impuesto + aumEjes + aumPasa;
//        System.out.println("Costo Final: " + cf);
//        int nota = Integer.parseInt(JOptionPane.showInputDialog("Digite su nota"));
//        if (nota >= 0 && nota <= 100) {
//            if (nota >= 90) {
//                System.out.println("Sobresaliente"); // 2/1500
//            } else if (nota >= 80) {
//                System.out.println("Notable"); // 4/1500
//            } else if (nota >= 70) {
//                System.out.println("Bien"); //10/1500
//            } else {
//                System.out.println("Insuficiente"); //4/1500
//            }
//        } else {
//            System.out.println("Nota inválida");
//        }
//        
//        //Alternativa
//        if (nota < 70) {
//            System.out.println("Insuficiente");
//        } else if (nota < 80) {
//            System.out.println("Bien");
//        } else if (nota < 90) {
//            System.out.println("Notable");
//        } else {
//            System.out.println("Sobresaliente");
//        } 
//        int par = Integer.parseInt(JOptionPane.showInputDialog("Número de Partituras"));
//        int can = Integer.parseInt(JOptionPane.showInputDialog("Número de Canciones"));
//
//        if (can >= 7 && can <= 10 && par == 0) {
//            System.out.println("Naciente");
//        } else if (can >= 7 && can <= 10 && par >= 1 && par <= 5) {
//            System.out.println("Estelar");
//        } else if (can > 10 && par > 5) {
//            System.out.println("Consagrado");
//        } else {
//            System.out.println("Formación");
//        }
//
//        //Alternativa
//        if (can >= 7 && can <= 10) {
//            if (par == 0) {
//                System.out.println("Naciente");
//            } else if (par >= 1 && par <= 5) {
//                System.out.println("Estelar");
//            } else {
//                System.out.println("Formación");
//            }
//        } else if (can > 10 && par > 5) {
//            System.out.println("Consagrado");
//        } else {
//            System.out.println("Formación");
//        }
// 500, 200, 100, 50, 20, 10, 5, 2 y 1
//        2
// 434 -> 34
//    
//    434|200
//    400 2 --> /
//    034 ---> %
//        int monto = 888;
//
//        int moneda = 500;
//        if (monto >= moneda) {
//            int mon = monto / moneda;
//            //monto = monto - mon * moneda;
//            monto = monto % moneda;
//            System.out.println(mon + " billetes de " + moneda);
//        }
//
//        moneda = 200;
//        if (monto >= moneda) {
//            int mon = monto / moneda;
//            //monto = monto - mon * moneda;
//            monto = monto % moneda;
//            System.out.println(mon + " billetes de " + moneda);
//        }
//
//        moneda = 100;
//        if (monto >= moneda) {
//            int mon = monto / moneda;
//            //monto = monto - mon * moneda;
//            monto = monto % moneda;
//            System.out.println(mon + " billetes de " + moneda);
//        }
//
//        moneda = 50;
//        if (monto >= moneda) {
//            int mon = monto / moneda;
//            //monto = monto - mon * moneda;
//            monto = monto % moneda;
//            System.out.println(mon + " billetes de " + moneda);
//        }
//
//        moneda = 20;
//        if (monto >= moneda) {
//            int mon = monto / moneda;
//            //monto = monto - mon * moneda;
//            monto = monto % moneda;
//            System.out.println(mon + " billetes de " + moneda);
//        }
//
//        moneda = 10;
//        if (monto >= moneda) {
//            int mon = monto / moneda;
//            //monto = monto - mon * moneda;
//            monto = monto % moneda;
//            System.out.println(mon + " billetes de " + moneda);
//        }
//
//        moneda = 5;
//        if (monto >= moneda) {
//            int mon = monto / moneda;
//            //monto = monto - mon * moneda;
//            monto = monto % moneda;
//            System.out.println(mon + " billetes de " + moneda);
//        }
//
//        moneda = 2;
//        if (monto >= moneda) {
//            int mon = monto / moneda;
//            //monto = monto - mon * moneda;
//            monto = monto % moneda;
//            System.out.println(mon + " monedas de " + moneda);
//        }
//
//        if (monto > 0) {
//            System.out.println(monto + " moneda de 1");
//        }
//        int valor = Integer.parseInt(JOptionPane.showInputDialog("Digite la edad del viajero"));
//        int personas = Integer.parseInt(JOptionPane.showInputDialog("Digite la edad del viajero"));
//
//        if (personas > 0) {
//            if (personas == 1) { //Viajan Solos 
//                int edad = Integer.parseInt(JOptionPane.showInputDialog("Digite la edad del viajero"));
//                if (edad >= 18 && edad <= 30) {
//                    System.out.println("Valor: " + valor);
//                    double desc = valor * 0.078;
//                    System.out.println("Desc.: " + desc);
//                    System.out.println("Total: " + (valor - desc));
//                } else if (edad > 30) {
//                    System.out.println("Valor: " + valor);
//                    double desc = valor * 0.10;
//                    System.out.println("Desc.: " + desc);
//                    System.out.println("Total: " + (valor - desc));
//                } else {
//                    System.out.println("Total: " + valor);
//                }
//            } else if (personas == 2) {
//                System.out.println("Valor:\t" + valor);
//                double desc = valor * 0.115;
//                System.out.println("Desc.:\t" + desc);
//                System.out.println("SubTot:\t" + (valor - desc) + "pp");
//                System.out.println("Total:\t" + (valor - desc) * personas);
//            } else if (personas > 3) {
//                System.out.println("Valor:\t" + valor);
//                double desc = valor * 0.15;
//                System.out.println("Desc.:\t" + desc);
//                System.out.println("SubTot:\t" + (valor - desc) + "pp");
//                System.out.println("Total:\t" + (valor - desc) * personas);
//            } else {
//                System.out.println("Valor:\t" + valor);
//                System.out.println("Total:\t" + valor * personas);
//            }
//        }
    }
}