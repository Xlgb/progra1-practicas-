/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package expresiones;

/**
 *
 * @author dell
 */
public class Main {
    public static void main(String[] args) {
        //Estructuras de control
        int a = 9;
        int b = 11;
        System.out.println("a==b: "+ (a == b));
        System.out.println("a!=b: "+ (a != b));
        System.out.println("a > b: "+ (a > b));
        System.out.println("a < b: "+ (a < b));
        System.out.println("a >= b: "+ (a >= b));
        System.out.println("a <= b: "+ (a <= b));
        
       //Operadores logicos
       //AND = &&
        System.out.println("true && true: "+(true && true));
        System.out.println("true && false: "+(true && false));
        System.out.println("false && true: "+(false && true));
        System.out.println("false && false: "+(false && false));
        //OR ||
        System.out.println("true || true: "+(true || true));
        System.out.println("true || false: "+(true || false));
        System.out.println("false || true: "+(false || true));
        System.out.println("false || false: "+(false || false));
        
        //NOT !
        
        System.out.println(" !true : "+(!true));
        System.out.println( "!false: "+(!false));
        
        
    }
    
}
