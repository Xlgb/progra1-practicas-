/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplogarrobos;

/**
 *
 * @author dell
 */
public class Persona {
    //Atrivutos
    public String nombre;
    public String estadoCivil;
    public String nacionalidad;
    public String idioma;
    public int cedula;
    public int fechaNacimiento;
    
    //Constructores
    public Persona(){
        
    }
    public Persona(String nombre, String estadoCivil, String nacionalidad, String idioma, int cedula, int fechaNacimiento){
        this.nombre = nombre;
        this.estadoCivil = estadoCivil;
        this.nacionalidad = nacionalidad;
        this.idioma = idioma;
        this.cedula = cedula;
        this.fechaNacimiento = fechaNacimiento;
    }
    public Persona(int cedula){
        this.cedula = cedula;
    }
    //Algunos otros mètodos----> no siempre
    //Getters & Setters

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEstadoCivil() {
        return estadoCivil;
    }

    public void setEstadoCivil(String estadoCivil) {
        this.estadoCivil = estadoCivil;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public String getIdioma() {
        return idioma;
    }

    public void setIdioma(String idioma) {
        this.idioma = idioma;
    }

    public int getCedula() {
        return cedula;
    }

    public void setCedula(int cedula) {
        this.cedula = cedula;
    }

    public int getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(int fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
    
     //ToStrings

    @Override
    public String toString() {
        return "Persona{" + "nombre=" + nombre + ", estadoCivil=" + estadoCivil + ", nacionalidad=" + nacionalidad + ", idioma=" + idioma + ", cedula=" + cedula + ", fechaNacimiento=" + fechaNacimiento + '}';
    }
    
    
    
}
