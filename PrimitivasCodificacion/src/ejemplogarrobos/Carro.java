/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplogarrobos;

/**
 *
 * @author dell
 */
public class Carro {
    //Atrivutos
    private String placa;
    private String marca;
    private String modelo;
    private int anno;
    private char transmision;
    private double precio;
    //Constructores
    public Carro(){
        
    }
    public Carro(String placa, String marca, String modelo, int anno, char transmision, double precio ){
        this.placa = placa;
        this.marca = marca;
        this.modelo = modelo;
        this.anno = anno;
        this.transmision = transmision;
        this.precio = precio;
    }
    //Algunos otros mètodos----> no siempre
    //Getters & Setters

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public char getTransmision() {
        return transmision;
    }

    public void setTransmision(char transmision) {
        this.transmision = transmision;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
     //ToStrings

    @Override
    public String toString() {
        return "Carro{" + "placa = " + placa + ", marca = " + marca + ", modelo = " + modelo + ", Año = " + anno + ", transmision = " + transmision + ", precio = " + precio + '}';
    }
    
}

