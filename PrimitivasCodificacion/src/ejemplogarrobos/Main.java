/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplogarrobos;

/**
 *
 * @author allanmual
 */
public class Main {
    //psvm + tab
    public static void main(String[] args) {
        System.out.println("Ejemplo de los Garrobos\n");
        //Nombre de la clase: Singular y Mayuscula(primera letra)
        
        //Instancia/Objeto de Garrobo
        Garrobo g1 = new Garrobo("Garrobito"); //g1 es la instancia, Aquí se crea una instancia de tipo Garrobo(es una clase)
        Garrobo g2 = new Garrobo("Juancito"); //g1 es la instancia, Aquí se crea una instancia de tipo Garrobo(es una clase)
        
        
        Garrobo g3 = new Garrobo("Pedrito");
        Garrobo g4 = new Garrobo("Marito", 20, 5);
        
        System.out.println(g1);
        System.out.println(g2);
        System.out.println(g3);
        System.out.println(g4);
        
        g1.setDistancia(20);
        g1.setTiempo(5);
        
        g2.setDistancia(25);
        g2.setTiempo(4);
        
//        System.out.println("Nombre\t\tTiempo\tDistancia");
//        System.out.println(g1.getNombre() + "\t"  + g1.getTiempo() + "\t" + g1.getDistancia());
//        System.out.println(g2.getNombre() + "\t"  + g2.getTiempo() + "\t" + g2.getDistancia());
//        
//        
//        System.out.println("\n\nAlternativa");
//        System.out.println("Nombre: " + g1.getNombre() + "\tTiempo: " + g1.getTiempo() + "\tDist. " + g1.getDistancia());
//        System.out.println("Nombre: " + g2.getNombre() + "\tTiempo: " + g2.getTiempo() + "\tDist. " + g2.getDistancia());
        
        System.out.println(g1);
        System.out.println(g2.toString()); //No es necesario llamarlo explicicitamente
        
        System.out.println("Hola!!! mi nombre " + g4.getNombre() + " Puedo recorrer " + g4.calcularDistancia(50) + "m por seg ");
        System.out.println("Hola!!! mi nombre " + g4.getNombre() + " Puedo recorrer " + g4.calcularTiempo(40) + "seg puedo recorrer  50m ");
     
        
    }
}
  
