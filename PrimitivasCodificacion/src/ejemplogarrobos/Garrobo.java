/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplogarrobos;

/**
 *
 * @author allanmual
 */
public class Garrobo {

    //Atributos, caracteristicas, tienen que ser privados. 
    private String nombre;
    private int distancia; //Distancia que recorrió
    private int tiempo; // tiempo en que recorrió esa distancia, según la investigación

    //Constructores

    public Garrobo() {
        
    }
    
    public Garrobo(String nombre) {
        this.nombre = nombre;
    }

    public Garrobo(String nombre, int distancia, int tiempo) {
        this.nombre = nombre;
        this.distancia = distancia;
        this.tiempo = tiempo;
    }

     

    //Otros Métodos 
    /**
     * Calcula la distancia que recorre el Garrovbo en el tiempo recibido por parametro
     * @param tiempo int con el tiempo en segundos a evaluar
     * @return double distancia en metros que recorre en el tiempo deseado
     */
    public double calcularDistancia(int tiempo){
        
        double res = (double)distancia * tiempo / this.tiempo;
        return res;
    }
    public double calcularTiempo(int distancia){
        double res = (double)tiempo * distancia / this.distancia;
        return res;
    }
    
    //Getters & Setters alt + insert, click derecho + insert code
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getDistancia() {
        return distancia;
    }

    public void setDistancia(int distancia) {
        this.distancia = distancia;
    }

    public int getTiempo() {
        return tiempo;
    }

    public void setTiempo(int tiempo) {
        this.tiempo = tiempo;
    }

    //toString para conocer el estado del objeto 
    @Override
    public String toString() {
        return "Garrobo{" + "nombre=" + nombre + ", distancia=" + distancia + ", tiempo=" + tiempo + '}';
    }

}