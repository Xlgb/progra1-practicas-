/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplogarrobos;

/**
 *
 * @author dell
 */
public class Computadora {
    public String marca ;
    public String tipo;
    public String modelo;
    public int precio;
    public String procesador;
    public String dueno;
    
    //Constructor
    public Computadora(){
        
    }
    public Computadora(String marca, String tipo, String modelo, int precio, String procesador, String dueno){
        this.marca = marca;
        this.tipo = tipo;
        this.modelo = modelo;
        this.precio = precio;
        this.procesador = procesador;
        this.dueno = dueno;
        
    }
    public Computadora(String dueno){
        this.dueno = dueno;
    }
    //Get & Set
    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public String getProcesador() {
        return procesador;
    }

    public void setProcesador(String procesador) {
        this.procesador = procesador;
    }

    public String getDueno() {
        return dueno;
    }

    public void setDueno(String dueno) {
        this.dueno = dueno;
    }
    //ToString
    @Override
    public String toString() {
        return "Computadora{" + "marca=" + marca + ", tipo=" + tipo + ", modelo=" + modelo + ", precio=" + precio + ", procesador=" + procesador + ", dueno=" + dueno + '}';
    }
    
    
            
}
